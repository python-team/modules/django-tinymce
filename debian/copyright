Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: django-tinymce
Upstream-Contact: Aljosa Mohorovic <aljosa.mohorovic@gmail.com>
Source: https://pypi.python.org/pypi/django-tinymce

Files: *
Copyright: 2008-2017, Aarni Koskela <akx@iki.fi>
           2008-2017, Aitzol Naberan <anaberan@codesyntax.com>
           2008-2017, Aljosa Mohorovic <aljosa.mohorovic@gmail.com>
           2008-2017, Andy Venet <avenet@users.noreply.github.com>
           2008-2017, BakaNeko <meganekko@list.ru>
           2008-2017, Chem <Chemt@ukr.net>
           2008-2017, Claude Paroz <claude@2xlibre.net>
           2008-2017, Craig de Stigter <craig.destigter@koordinates.com>
           2008-2017, Dan Moore <dan@moore.cx>
           2008-2017, David Harks <dave@dwink.net>
           2008-2017, Fred Stluka <fred@bristle.com>
           2008-2017, Guilherme Rezende <guilhermebr@gmail.com>
           2008-2017, Hugo Des <hugo.deslongchamps@smile.fr>
           2008-2017, Ian Bruce <ian@projectbruce.net>
           2008-2017, Jaap Roes <jaap@eight.nl>
           2008-2017, James Cleveland <jamescleveland@gmail.com>
           2008-2017, Jason Davies <jason@jasondavies.com>
           2008-2017, Jessamyn Smith <jessamyn.smith@gmail.com>
           2008-2017, Joel Burton <joel@joelburton.com>
           2008-2017, Jonathan D. Baker <jonathandavidbaker@gmail.com>
           2008-2017, Joost Cassee <joost@cassee.net>
           2008-2017, Lee Semel <lee@semel.net>
           2008-2017, Matt Archibald <marchiba@u.rochester.edu>
           2008-2017, Peter van Kampen <pterk@datatailors.com>
           2008-2017, Piet Delport <piet@byteorbit.com>
           2008-2017, Pokutnik Alexandr <pokutnik@gmail.com>
           2008-2017, Quadracik <quadracik@gmail.com>
           2008-2017, Rémy HUBSCHER <rhubscher@mozilla.com>
           2008-2017, Steffen Jasper <jesus_at_hollywoodparty@gmx.net>
           2008-2017, Tim Saylor <tim.saylor@gmail.com>
           2008-2017, Venelin Stoykov <vkstoykov@gmail.com>
           2008-2017, Zuzel Vera <zuzel.vp@gmail.com>
           2008-2017, Ivan Chernov <chernoffivan@gmail.com>
           2008-2017, Andrew Meakovski <meako689@gmail.com>
           2008-2017, Tom Chiung-ting Chen <ctchen@gmail.com>
           2008-2017, Dima Revutskyi <rddimon@gmail.com>
License: EXPAT

Files: debian/*
Copyright: 2017, Hans-Christoph Steiner
License: EXPAT

License: EXPAT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
